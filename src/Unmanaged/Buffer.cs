﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using System;
using System.Text;

namespace PaperHat.Engine.Unmanaged
{
    public unsafe struct Buffer<TData> : IEquatable<Buffer<TData>> where TData : unmanaged, IStructData
    {
        public int Position;
        public TData _Data;

        public int Length => _Data.Length;

        public int Capacity => _Data.Capacity;

        public int Remaining => _Data.Capacity - _Data.Length;

        public bool ReadBool()
        {
            var value = *(bool*) (_Data.Pointer + Position);
            Position += sizeof(bool);
            return value;
        }

        public byte ReadByte()
        {
            var value = *(byte*)(_Data.Pointer + Position);
            Position += sizeof(byte);
            return value;
        }

        public sbyte ReadSByte()
        {
            var value = *(sbyte*)(_Data.Pointer + Position);
            Position += sizeof(sbyte);
            return value;
        }

        public char ReadChar()
        {
            var value = *(char*)(_Data.Pointer + Position);
            Position += sizeof(char);
            return value;
        }

        public short ReadShort()
        {
            var value = *(short*)(_Data.Pointer + Position);
            Position += sizeof(short);
            return value;
        }

        public ushort ReadUShort()
        {
            var value = *(ushort*)(_Data.Pointer + Position);
            Position += sizeof(ushort);
            return value;
        }

        public int ReadInt()
        {
            var value = *(int*)(_Data.Pointer + Position);
            Position += sizeof(int);
            return value;
        }

        public uint ReadUInt()
        {
            var value = *(uint*)(_Data.Pointer + Position);
            Position += sizeof(uint);
            return value;
        }

        public float ReadFloat()
        {
            var value = *(float*)(_Data.Pointer + Position);
            Position += sizeof(float);
            return value;
        }

        public double ReadDouble()
        {
            var value = *(double*)(_Data.Pointer + Position);
            Position += sizeof(double);
            return value;
        }

        public long ReadLong()
        {
            var value = *(long*)(_Data.Pointer + Position);
            Position += sizeof(long);
            return value;
        }

        public ulong ReadULong()
        {
            var value = *(ulong*)(_Data.Pointer + Position);
            Position += sizeof(ulong);
            return value;
        }

        public string ReadString(Encoding encoding = null)
        {
            int length = ReadInt();
            if (length == 0)
                return null;
            if (encoding == null)
                encoding = Encoding.UTF8;
            var value = encoding.GetString(_Data.Pointer + Position, length);
            Position += length;
            return value;
        }

        public T Read<T>() where T : unmanaged
        {
            var value = *(T*)(_Data.Pointer + Position);
            Position += sizeof(T);
            return value;
        }

        public T ReadData<T>() where T : unmanaged, IStructData
        {
            T value = default;
            value.Length = ReadInt();
            System.Buffer.MemoryCopy(_Data.Pointer + _Data.Length, value.Pointer, value.Capacity, value.Length);
            Position += value.Length;
            return value;
        }

        public Buffer<T> ReadBuffer<T>() where T : unmanaged, IStructData
        {
            T data = ReadData<T>();
            return new Buffer<T>() {_Data = data};
        }

        public void WriteBool(bool value)
        {
            *(bool*) (_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(bool);
        }

        public void WriteByte(byte value)
        {
            *(byte*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(byte);
        }

        public void WriteSByte(sbyte value)
        {
            *(sbyte*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(sbyte);
        }

        public void WriteChar(char value)
        {
            *(char*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(char);
        }

        public void WriteShort(short value)
        {
            *(short*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(short);
        }

        public void WriteUShort(ushort value)
        {
            *(ushort*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(ushort);
        }

        public void WriteInt(int value)
        {
            *(int*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(int);
        }

        public void WriteUInt(uint value)
        {
            *(uint*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(uint);
        }

        public void WriteFloat(float value)
        {
            *(float*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(float);
        }

        public void WriteDouble(double value)
        {
            *(double*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(double);
        }

        public void WriteLong(long value)
        {
            *(long*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(long);
        }

        public void WriteULong(ulong value)
        {
            *(ulong*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(ulong);
        }

        public void WriteString(string value, Encoding encoding = null)
        {
            if (value == null)
            {
                WriteInt(0);
                return;
            }
            if (encoding == null)
                encoding = Encoding.UTF8;
            int length = encoding.GetByteCount(value);
            WriteInt(length);
            fixed(char* c = value)
                encoding.GetBytes(c, value.Length, _Data.Pointer + _Data.Length, _Data.Capacity - _Data.Length);
            _Data.Length += length;
        }

        public void Write<T>(T value) where T : unmanaged
        {
            *(T*)(_Data.Pointer + _Data.Length) = value;
            _Data.Length += sizeof(T);
        }

        public void WriteData<T>(T value) where T : unmanaged, IStructData
        {
            WriteInt(value.Length);
            System.Buffer.MemoryCopy(value.Pointer, _Data.Pointer + _Data.Length, _Data.Capacity, value.Length);
            _Data.Length += value.Length;
        }

        public void WriteBuffer<T>(Buffer<T> buffer) where T : unmanaged, IStructData
        {
            WriteData(buffer._Data);
        }

        public bool Equals<TData1>(TData1 other) where TData1 : unmanaged, IStructData
        {
            return _Data.Equals(other);
        }

        public bool Equals(Buffer<TData> other)
        {
            return _Data.Equals(other._Data);
        }

        public override bool Equals(object obj)
        {
            return obj is Buffer<TData> other && Equals(other);
        }

        public override int GetHashCode()
        {
            return _Data.GetHashCode();
        }
    }
}
