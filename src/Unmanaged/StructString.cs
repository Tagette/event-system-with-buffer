﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using System.Runtime.InteropServices;
using System.Text;

namespace PaperHat.Engine.Unmanaged
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct StructString<TData> where TData : unmanaged, IStructData
    {
        public TData _Data;

        public string CreateString()
        {
            return Encoding.UTF8.GetString(_Data.Pointer, _Data.Length);
        }

        public static implicit operator StructString<TData>(string value)
        {
            var str = new StructString<TData>();
            fixed (char* c = value)
                str._Data.Length = Encoding.UTF8.GetBytes(c, value.Length, str._Data.Pointer, str._Data.Capacity);
            return str;
        }

        public bool Equals(StructString<TData> other)
        {
            return _Data.Equals(other._Data);
        }

        public override bool Equals(object obj)
        {
            return obj is StructString<TData> other && Equals(other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(_Data);
        }

        public static bool operator ==(StructString<TData> a, StructString<TData> b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(StructString<TData> a, StructString<TData> b)
        {
            return !a.Equals(b);
        }
    }
}
