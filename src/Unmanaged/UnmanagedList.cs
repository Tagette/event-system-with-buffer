﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using PaperHat.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Unity.Collections.LowLevel.Unsafe;

namespace PaperHat.Engine.Unmanaged
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct UnmanagedList<T, TData> where T : unmanaged where TData : unmanaged, IStructData
    {
        public int _Count;
        public TData _Data;

        public int Capacity => _Data.Capacity / sizeof(T);

        public T this[int index] => *(T*)(_Data.As<T>() + index);

        public T Get(int index) => *(_Data.As<T>() + index);

        public void Add(T value) => *(_Data.As<T>() + _Count++) = value;

        public void AddRange(IEnumerable<T> range)
        {
            if (range is FastList<T> fastList)
            {
                if (fastList.Count > (Capacity - _Count))
                    throw new InvalidOperationException("Not enough space left in the data to insert the provided range.");

                fixed (T* d = fastList._Data)
                {
                    UnsafeUtility.MemCpy((_Data.As<T>() + _Count), d, fastList.Count * sizeof(T));
                    _Count += fastList.Count;
                }
            }
            else if (range is IList<T> list)
            {
                if (list.Count > Capacity - _Count)
                    throw new InvalidOperationException("Not enough space left in the data to insert the provided range.");

                int count = list.Count;
                for (int i = 0; i < count; i++)
                {
                    Add(list[i]);
                }
            }
            else
            {
                foreach (var value in range)
                {
                    if(Capacity == _Count)
                        throw new InvalidOperationException("Not enough space left in the data to insert the remainder of the provided range.");

                    Add(value);
                }
            }
        }

        public void AddRange<TData2>(UnmanagedList<T, TData2> list) where TData2 : unmanaged, IStructData
        {
            if (list._Count > (Capacity - _Count))
                throw new InvalidOperationException("Not enough space left in the data to insert the provided range.");

            UnsafeUtility.MemCpy(_Data.As<T>() + _Count, list._Data.As<T>(), list._Count * sizeof(T));
            _Count += list._Count;
        }

        public void Insert(int index, T value)
        {
            var size = sizeof(T);
            if (index < _Count)
                UnsafeUtility.MemCpy(_Data.Pointer + (index * size), _Data.Pointer + ((index + 1) * size), (_Count - index) * size);

            *(T*) (_Data.Pointer + (index * size)) = value;
            _Count++;
        }

        public void Remove(T value)
        {
            var size = sizeof(T);
            for (int i = 0; i < _Count; i++)
            {
                var each = *(T*) (_Data.Pointer + (i * size));
                if (each.Equals(value))
                {
                    RemoveAt(i);
                }
            }
        }

        public T RemoveAt(int index)
        {
            var size = sizeof(T);
            T value = *(T*) (_Data.Pointer + (index * size));
            if (index < _Count - 1)
                UnsafeUtility.MemCpy(_Data.Pointer + ((index + 1) * size), _Data.Pointer + (index * size), (_Count - index - 1) * size);
            _Count--;
            return value;
        }

        public int IndexOf(T value)
        {
            var size = sizeof(T);
            for (int i = 0; i < _Count; i++)
            {
                var each = *(T*) (_Data.Pointer + (i * size));
                if (each.Equals(value))
                    return i;
            }

            return -1;
        }

        public void Clear()
        {
            _Count = 0;
            UnsafeUtility.MemClear(_Data.Pointer, _Count * sizeof(T));
        }
    }
}
