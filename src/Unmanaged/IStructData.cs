﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

namespace PaperHat.Engine.Unmanaged
{
    public interface IStructData : IStructEquatable
    {
        int Length { get; set; }
        int Capacity { get; }
        unsafe byte* Pointer { get; }

        unsafe T* As<T>() where T : unmanaged;
    }
}
