﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using PaperHat.Engine.Unmanaged;
using Unity.Collections.LowLevel.Unsafe;

namespace PaperHat.Engine.Structs
{
    public static class StructDataExtensions
    {
        /// <summary>
        /// Gets a copy of the struct data with all bits set to 1.
        /// </summary>
        public static unsafe T GetMaxValue<T>(this T data) where T : unmanaged, IStructData
        {
            var d = data.Pointer;
            UnsafeUtility.MemSet(d, byte.MaxValue, data.Capacity);

            return data;
        }
    }
}
