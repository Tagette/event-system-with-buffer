﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using System;
using System.Runtime.InteropServices;

namespace PaperHat.Engine.Unmanaged
{
    public class ByteComparer
    {
        public static unsafe bool Equals<TData1, TData2>(TData1 a, TData2 b)
            where TData1 : unmanaged, IStructData
            where TData2 : unmanaged, IStructData
        {
            if (a.Length != b.Length)
                return false;

            if (a.Length == 1)
                return *(a.Pointer) == *(b.Pointer);

            if (a.Length == 2)
                return *(short*)a.Pointer == *(short*)b.Pointer;

            if (a.Length == 4)
                return *(int*)a.Pointer == *(int*)b.Pointer;

            if (a.Length == 8)
                return *(long*)a.Pointer == *(long*)b.Pointer;

            if (a.Length == 16)
                return *(decimal*)a.Pointer == *(decimal*)b.Pointer;

            // We choose to iterate the data without comparing the hashcodes because getting the hashcode iterates the
            // data anyways. It also has to iterate both sets of data to generate a hashcode for both. This avoids
            // that by only iterating the data once and actually compares the data.

            // TODO: Optimize by caching the hashcode and comparing those first.

            int index = 0;
            int len = a.Length;
            while (len >= 8)
            {
                long val1 = *(long*)a.Pointer[index / 8];
                long val2 = *(long*)b.Pointer[index / 8];

                if (val1 != val2)
                    return false;

                index += 8;
                len -= 8;
            }

            while (len > 0)
            {
                if (a.Pointer[index] != b.Pointer[index])
                    return false;

                index++;
                len--;
            }

            return true;
        }

        public static unsafe int GetHashcode<TData>(TData data) where TData : unmanaged, IStructData
        {
            if (data.Length == 1)
                return (*data.Pointer).GetHashCode();

            if (data.Length == 2)
                return (*(short*) data.Pointer).GetHashCode();

            if (data.Length == 4)
                return (*(int*) data.Pointer).GetHashCode();

            if (data.Length == 8)
                return (*(long*) data.Pointer).GetHashCode();

            if (data.Length == 16)
                return (*(decimal*) data.Pointer).GetHashCode();

            // TODO: Will there be more frequent collisions because we use a different hashcode for 16 bytes or less?

            // TODO: Finish this optimization. Cache the hashcode.
            // TODO: What if the value is non-zero but not the hashcode?
            //if (Length < Capacity - 4)
            //{
            //    int hash = *(int*)b[Capacity - 4];
            //    if (hash != 0)
            //        return hash;
            //}

            // Hashing algorithm from 32 bit c# string. Modified to work for bytes.
            int hash1 = (5381 << 16) + 5381;
            int hash2 = hash1;

            int* pint = (int*) data.Pointer;
            int len = data.Length;
            while (len >= 8)
            {
                hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ pint[0];
                hash2 = ((hash2 << 5) + hash2 + (hash2 >> 27)) ^ pint[1];
                pint += 2;
                len -= 8;
            }

            while (len >= 4)
            {
                hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ pint[0];
                pint++;
                len -= 4;
            }

            byte* pbyte = (byte*) pint;
            while (len > 0)
            {
                hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ pbyte[0];
                pbyte++;
                len--;
            }

            return hash1 + (hash2 * 1566083941);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte4 : IStructData, IEquatable<Byte4>
    {
        public const int CAPACITY = 4;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*) d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable<Byte4>

        public bool Equals(Byte4 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable<Byte4>

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte8 : IStructData, IEquatable<Byte8>
    {
        public const int CAPACITY = 8;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte8 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte16 : IStructData, IEquatable<Byte16>
    {
        public const int CAPACITY = 16;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte16 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte32 : IStructData, IEquatable<Byte32>
    {
        public const int CAPACITY = 32;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte32 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte64 : IStructData, IEquatable<Byte64>
    {
        public const int CAPACITY = 64;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte64 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte128 : IStructData, IEquatable<Byte128>
    {
        public const int CAPACITY = 128;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte128 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte256 : IStructData, IEquatable<Byte256>
    {
        public const int CAPACITY = 256;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte256 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte512 : IStructData, IEquatable<Byte512>
    {
        public const int CAPACITY = 512;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte512 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte1024 : IStructData, IEquatable<Byte1024>
    {
        public const int CAPACITY = 1024;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte1024 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte2048 : IStructData, IEquatable<Byte2048>
    {
        public const int CAPACITY = 2048;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte2048 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte4096 : IStructData, IEquatable<Byte4096>
    {
        public const int CAPACITY = 4096;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte4096 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte8192 : IStructData, IEquatable<Byte8192>
    {
        public const int CAPACITY = 8192;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte8192 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct Byte16384 : IStructData, IEquatable<Byte16384>
    {
        public const int CAPACITY = 16384;

        public int _Length;
        public fixed byte _Data[CAPACITY];

        public int Length
        {
            get => _Length;
            set => _Length = value;
        }

        public int Capacity => CAPACITY;

        public byte* Pointer
        {
            get
            {
                fixed (byte* d = _Data) return d;
            }
        }

        public T* As<T>() where T : unmanaged
        {
            fixed (byte* d = _Data)
                return (T*)d;
        }

        #region IStructEquatable

        public bool Equals<TData>(TData other) where TData : unmanaged, IStructData
        {
            return ByteComparer.Equals(this, other);
        }

        #endregion // IStructEquatable

        #region IEquatable

        public bool Equals(Byte16384 other)
        {
            return ByteComparer.Equals(this, other);
        }

        public override int GetHashCode()
        {
            return ByteComparer.GetHashcode(this);
        }

        #endregion // IEquatable

        public override bool Equals(object obj)
        {
            return obj is IStructEquatable other && other.Equals(this);
        }
    }
}
