﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

namespace PaperHat.Events
{
    public struct EventField<T> where T : struct, IEvent
    {
        public void Subscribe(EventSubscriber<T> subscriber)
        {
            G.Event.Subscribe<T>(subscriber);
        }

        public void Unsubscribe(EventSubscriber<T> subscriber)
        {
            G.Event.Subscribe<T>(subscriber);
        }
    }
}
