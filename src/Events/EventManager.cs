﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using PaperHat.Reflection;
using System;
using System.Collections.Generic;
using PaperHat.Collections;
using PaperHat.Systems;

namespace PaperHat.Events
{

    public class EventManager : BaseSystem, IEventManager
    {
        protected delegate void ComponentRaise(ref EventComponents components);

        protected struct SubscriptionInfo
        {
            public HashSet<Delegate> CheckIn;
            public Delegate SubscriberInvoke;
            public ComponentRaise ComponentInvoke;
            public Dictionary<Delegate, SubscriberInfo> Subscribers;
        }

        protected struct SubscriberInfo
        {
            public SubscriberInfo(Delegate subscriber, Delegate invoke)
            {
                Subscriber = subscriber;
                Invoke = invoke;
            }

            public Delegate Subscriber;
            public Delegate Invoke;
        }

        protected bool _isRaising;
        protected FastList<Action> _QueuedRaises = new FastList<Action>();
        protected readonly Dictionary<TypeID, SubscriptionInfo> _Subscribers = new Dictionary<TypeID, SubscriptionInfo>();

        public override void Initialize()
        {
        }

        public override void Uninitialize()
        {
            _isRaising = false;
            _QueuedRaises.Clear();
            _Subscribers.Clear();
        }

        public virtual void Raise<T>(Event<T> evt) where T : struct, IEvent
        {
            // TODO: Make thread safe.

            if (_isRaising)
            {
                _QueuedRaises.Add(() => RaiseInternal(evt));
                return;
            }

            RaiseInternal(evt);

            for (int i = 0; i < _QueuedRaises._Count; i++)
            {
                var queued = _QueuedRaises._Data[i];
                queued();
            }
            _QueuedRaises.Clear();
        }

        private void RaiseInternal<T>(Event<T> evt) where T : struct, IEvent
        {
            _isRaising = true;

            if (_Subscribers.TryGetValue(evt.TypeID, out SubscriptionInfo info))
            {
                var subscribers = (EventSubscriber<T>)info.SubscriberInvoke;
                subscribers(ref evt);

                var raiseComponents = info.ComponentInvoke;
                if (raiseComponents != null)
                    raiseComponents(ref evt.Components);
            }

            _isRaising = false;
        }

        public virtual void Subscribe<T>(EventSubscriber<T> subscriber) where T : struct, IEvent
        {
            if (subscriber == null)
                return;

            var id = TypeID<T>.ID;

            void SafeRaise(ref Event<T> e)
            {
                try
                {
                    subscriber(ref e);
                }
                catch (Exception ex)
                {
                    ex.Data["Event"] = id;
                    G.Log.Exception(ex);
                }
            }

            void ComponentRaise(ref EventComponents components)
            {
                var headers = components.GetHeaders();
                for (int i = 0; i < headers._Count; i++)
                {
                    var header = headers[i];
                    if (_Subscribers.TryGetValue(header.TypeID, out SubscriptionInfo componentInfo))
                    {
                        var raiseComponents = componentInfo.ComponentInvoke;
                        if(raiseComponents != null)
                            raiseComponents(ref components);
                    }
                }
            }

            // First subscribe for this event type.
            if (!_Subscribers.TryGetValue(id, out SubscriptionInfo info))
            {
                _Subscribers[id] = new SubscriptionInfo()
                {
                    CheckIn = new HashSet<Delegate>(),
                    ComponentInvoke = ComponentRaise,
                    Subscribers = new Dictionary<Delegate, SubscriberInfo>(),
                };
            }

            // Ensure single subscription for a subscriber.
            if (!info.CheckIn.Contains(subscriber))
            {
                var invoke = (EventSubscriber<T>) SafeRaise;
                info.CheckIn.Add(subscriber);
                info.SubscriberInvoke = Delegate.Combine(info.SubscriberInvoke, invoke);
                info.Subscribers.Add(subscriber, new SubscriberInfo(subscriber, invoke));
            }
        }

        public virtual void Unsubscribe<T>(EventSubscriber<T> subscriber) where T : struct, IEvent
        {
            if (subscriber == null)
                return;

            var id = TypeID<T>.ID;
            if (_Subscribers.TryGetValue(id, out SubscriptionInfo subscription))
            {
                if (subscription.CheckIn.Contains(subscriber))
                {
                    subscription.CheckIn.Remove(subscriber);
                    if (subscription.Subscribers.TryGetValue(subscriber, out SubscriberInfo sub))
                    {
                        subscription.SubscriberInvoke = Delegate.Remove(subscription.SubscriberInvoke, sub.Invoke);
                        subscription.Subscribers.Remove(subscriber);
                    }
                }

                if (subscription.CheckIn.Count == 0)
                    _Subscribers.Remove(id);
            }
        }

        public virtual void Subscribe<T>(EventComponentSubscriber<T> subscriber) where T : unmanaged, IEventComponent
        {
            if (subscriber == null)
                return;

            var id = TypeID<T>.ID;

            void ComponentRaise(ref EventComponents components)
            {
                // TODO: Could slightly optimize this if we knew the byte index of the component.
                if (components.GetComponent(out T component))
                {
                    try
                    {
                        subscriber(ref component);

                        components.SetComponent(component);
                    }
                    catch (Exception ex)
                    {
                        ex.Data["Event"] = id;
                        G.Log.Exception(ex);
                    }
                }
            }

            if (!_Subscribers.TryGetValue(id, out SubscriptionInfo subscription))
            {
                _Subscribers[id] = new SubscriptionInfo()
                {
                    CheckIn = new HashSet<Delegate>(),
                    Subscribers = new Dictionary<Delegate, SubscriberInfo>(),
                };
            }

            if (!subscription.CheckIn.Contains(subscriber))
            {
                subscription.CheckIn.Add(subscriber);
                subscription.ComponentInvoke += ComponentRaise;
                subscription.Subscribers.Add(subscriber, new SubscriberInfo(subscriber, (ComponentRaise)ComponentRaise));
            }
        }

        public virtual void Unsubscribe<T>(EventComponentSubscriber<T> subscriber) where T : unmanaged, IEventComponent
        {
            if (subscriber == null)
                return;

            var id = TypeID<T>.ID;

            if (_Subscribers.TryGetValue(id, out SubscriptionInfo subscription))
            {
                if (subscription.CheckIn.Contains(subscriber))
                {
                    subscription.CheckIn.Remove(subscriber);
                    if (subscription.Subscribers.TryGetValue(subscriber, out SubscriberInfo sub))
                    {
                        subscription.SubscriberInvoke = Delegate.Remove(subscription.SubscriberInvoke, sub.Invoke);
                        subscription.Subscribers.Remove(subscriber);
                    }
                }

                if (subscription.CheckIn.Count == 0)
                    _Subscribers.Remove(id);
            }
        }
    }
}
