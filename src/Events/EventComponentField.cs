﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

namespace PaperHat.Events
{
    public struct EventComponentField<T> where T : unmanaged, IEventComponent
    {
        public void Subscribe(EventComponentSubscriber<T> subscriber)
        {
            G.Event.Subscribe<T>(subscriber);
        }

        public void Unsubscribe(EventComponentSubscriber<T> subscriber)
        {
            G.Event.Subscribe<T>(subscriber);
        }
    }
}
