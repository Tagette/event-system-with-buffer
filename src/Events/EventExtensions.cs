﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using PaperHat.Engine.Unmanaged;

namespace PaperHat.Events
{
    public struct NetworkComponent : IEventComponent
    {
        public int Sender;
    }

    public static class EventExtensions
    {
        public static void Raise<T>(this T e) where T : struct, IEvent
        {
            Event<T> evt = default;
            evt.Value = e;
            var manager = G.Event;
            if (manager == null)
            {
                G.Log.Error("Unable to find the EventManager in order to raise the {0} event.", evt.TypeID);
                return;
            }

            manager.Raise(evt);
        }

        public static void Send<T>(this T e, UnmanagedList<int, Byte1024> recipients)
            where T : struct, INetworkEvent
        {
            Event<T> evt = default;
            evt.Value = e;
            if (!evt.GetComponent(out NetworkComponent net))
                net = new NetworkComponent();

            net.Sender = default; // TODO: Local player.

            var buffer = new Buffer<Byte4096>();
            evt.SetComponent(net);
            e.Write(ref buffer);

            // TODO: Actually send.
        }
    }
}