﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using PaperHat.Engine.Unmanaged;

namespace PaperHat.Events
{
    public delegate void EventSubscriber<T>(ref Event<T> evt) where T : struct, IEvent;
    public delegate void EventComponentSubscriber<TComponent>(ref TComponent component) where TComponent : unmanaged, IEventComponent;

    public interface IEvent
    {
    }

    public interface INetworkEvent : IEvent
    {
        void Write<TData>(ref Buffer<TData> buffer) where TData : unmanaged, IStructData;
        void Read<TData>(ref Buffer<TData> buffer) where TData : unmanaged, IStructData;
    }

    public interface IEventComponent
    {
    }

    public interface IEventManager
    {
        void Raise<T>(Event<T> evt) where T : struct, IEvent;
        void Subscribe<T>(EventSubscriber<T> subscriber) where T : struct, IEvent;
        void Unsubscribe<T>(EventSubscriber<T> subscriber) where T : struct, IEvent;
        void Subscribe<T>(EventComponentSubscriber<T> subscriber) where T : unmanaged, IEventComponent;
        void Unsubscribe<T>(EventComponentSubscriber<T> subscriber) where T : unmanaged, IEventComponent;
    }
}
