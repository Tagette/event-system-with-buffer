﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

namespace PaperHat.Events
{
    public struct CancelComponent : IEventComponent
    {
        public bool IsCancelled;
    }
}
