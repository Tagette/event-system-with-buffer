﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using PaperHat.Reflection;
using PaperHat.Engine.Unmanaged;
using System;
using System.Runtime.InteropServices;

namespace PaperHat.Events
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EventComponentHeader : IEquatable<EventComponentHeader>
    {
        public TypeID TypeID;
        public byte Length;

        public bool Equals(EventComponentHeader other)
        {
            return TypeID.Equals(other.TypeID) && Length == other.Length;
        }

        public override bool Equals(object obj)
        {
            return obj is EventComponentHeader other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (TypeID.GetHashCode() * 397) ^ Length.GetHashCode();
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Event<T> where T : struct, IEvent
    {
        public T Value;
        public EventComponents Components;

        public TypeID TypeID => TypeID<T>.ID;

        /// <summary>
        /// Determines if the event contains the specified component.
        /// </summary>
        public bool HasComponent<TComponent>() where TComponent : unmanaged, IEventComponent
        {
            return Components.HasComponent<TComponent>();
        }

        /// <summary>
        /// Gets an event component from the given index.
        /// </summary>
        public bool GetComponent<TComponent>(out TComponent value) where TComponent : unmanaged, IEventComponent
        {
            return Components.GetComponent(out value);
        }


        /// <summary>
        /// Sets the event component at the given index.
        /// </summary>
        /// <param name="value">The component data.</param>
        public void SetComponent<TComponent>(TComponent value) where TComponent : unmanaged, IEventComponent
        {
            Components.SetComponent(value);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct EventComponents
    {
        private Byte4096 _Data;

        public UnmanagedList<EventComponentHeader, Byte128> GetHeaders()
        {
            var list = new UnmanagedList<EventComponentHeader, Byte128>();
            var headerSize = TypeID<EventComponentHeader>.Size;
            for (int i = 0; i < _Data._Length;)
            {
                var header = *(EventComponentHeader*)(_Data.Pointer + i);
                list.Add(header);

                i += headerSize + header.Length;
            }

            return list;
        }

        /// <summary>
        /// Determines if the event contains the specified component.
        /// </summary>
        public bool HasComponent<TComponent>() where TComponent : unmanaged, IEventComponent
        {
            var typeID = TypeID<TComponent>.ID;
            var headerSize = TypeID<EventComponentHeader>.Size;
            for (int i = 0; i < _Data._Length;)
            {
                var header = *(EventComponentHeader*)(_Data.Pointer + i);
                if (header.TypeID == typeID)
                    return true;

                i += headerSize + header.Length;
            }

            return false;
        }

        /// <summary>
        /// Gets an event component from the given index.
        /// </summary>
        public bool GetComponent<TComponent>(out TComponent value) where TComponent : unmanaged, IEventComponent
        {
            int headerSize = sizeof(EventComponentHeader);
            var typeID = TypeID<TComponent>.ID;

            for (int i = 0; i < _Data._Length;)
            {
                var header = *(EventComponentHeader*)(_Data.Pointer + i);
                if (header.TypeID == typeID)
                {
                    value = *(TComponent*)(_Data.Pointer + i + headerSize);
                    return true;
                }

                i += headerSize + header.Length;
            }

            value = default;
            return false;
        }


        /// <summary>
        /// Sets the event component at the given index.
        /// </summary>
        /// <param name="value">The component data.</param>
        public void SetComponent<TComponent>(TComponent value) where TComponent : unmanaged, IEventComponent
        {
            int headerSize = sizeof(EventComponentHeader);
            TypeID typeID = TypeID<TComponent>.ID;
            int valueSize = sizeof(TComponent);

            if (valueSize > sizeof(byte))
                throw new Exception("Event components must be less then 256 bytes.");

            for (int i = 0; i < _Data._Length;)
            {
                var header = *(EventComponentHeader*)(_Data.Pointer + i);
                if (header.TypeID == typeID)
                {
                    *(TComponent*)(_Data.Pointer + i) = value;
                    return;
                }

                i += headerSize + header.Length;
            }

            if (_Data._Length + valueSize + headerSize > _Data.Capacity)
                throw new Exception($"An event can hold a maximum of {_Data.Capacity} bytes for components. ({typeof(TComponent).Name})");

            *(EventComponentHeader*)(_Data.Pointer + _Data._Length) = new EventComponentHeader
            {
                TypeID = typeID,
                Length = (byte)valueSize,
            };
            *(TComponent*)(_Data.Pointer + _Data._Length + headerSize) = value;
            _Data._Length += (ushort)(headerSize + valueSize);
        }
    }
}
