﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using System;
using Unity.Collections.LowLevel.Unsafe;

namespace PaperHat.Reflection
{
    public struct TypeID : IEquatable<TypeID>
    {
        public static TypeID NoType = default;

        public TypeID(Type type)
        {
            _ID = type.GetIDForType()._ID;
        }

        public TypeID(int id)
        {
            _ID = id;
        }

        public readonly int _ID;

        /// <summary>
        /// Gets the type from the <see cref="ReflectionCache"/>.
        /// </summary>
        public Type Type => ReflectionCache.GetTypeFromID(this);

        /// <summary>
        /// Gets the size of the type in bytes.
        /// </summary>
        public int Size => ReflectionCache.GetTypeSize(this);

        /// <summary>
        /// Determines if the type <typeparamref name="T"/> is the same as this type.
        /// </summary>
        public bool Equals<T>()
        {
            return TypeID<T>.ID.Equals(_ID);
        }

        /// <summary>
        /// Determines if the type given is the same as this type.
        /// </summary>
        public bool Equals(Type type)
        {
            return type.GetIDForType()._ID == _ID;
        }

        /// <summary>
        /// Determines if this type id equals another type id.
        /// </summary>
        public bool Equals(TypeID other)
        {
            return _ID == other._ID;
        }

        /// <summary>
        /// Using this equals will box the type id.
        /// </summary>
        public override bool Equals(object obj)
        {
            return obj is TypeID other && Equals(other);
        }

        /// <summary>
        /// Returns the inner type id as a hashcode.
        /// </summary>
        public override int GetHashCode()
        {
            return _ID;
        }

        public static bool operator ==(TypeID a, TypeID b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(TypeID a, TypeID b)
        {
            return !a.Equals(b);
        }
    }

    public static class TypeID<T>
    {
        /// <summary>
        /// The system type of the type.
        /// </summary>
        public static Type Type = typeof(T);

        /// <summary>
        /// The <see cref="TypeID"/> for this type.
        /// </summary>
        public static TypeID ID = Type.GetIDForType();

        /// <summary>
        /// The unmanaged size of the type.
        /// </summary>
        public static int Size = UnsafeUtility.SizeOf(Type);
    }
}
