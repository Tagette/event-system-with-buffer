﻿/* 
Author: Tristan Chambers
License: MIT https://opensource.org/licenses/MIT
Copyright: 2020 Tristan Chambers
Repo: https://bitbucket.org/Tagette/event-system-with-buffer/src
*/

using PaperHat.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;
using Unity.Collections.LowLevel.Unsafe;

namespace PaperHat.Reflection
{
    public static class ReflectionCache
    {
        private static readonly FastList<Assembly> _assemblies = new FastList<Assembly>();
        private static readonly FastList<Type> _types = new FastList<Type>();

        private static readonly Dictionary<Type, TypeID> _typeForCode = new Dictionary<Type, TypeID>();
        private static readonly Dictionary<TypeID, Type> _codeForType = new Dictionary<TypeID, Type>();
        private static readonly Dictionary<TypeID, int> _typeSizes = new Dictionary<TypeID, int>();
        private static readonly Dictionary<Type, Type[]> _interfaces = new Dictionary<Type, Type[]>();
        private static readonly Dictionary<Type, IList<Type>> _implementors = new Dictionary<Type, IList<Type>>();

        public static void CacheCurrentAssembly()
        {
            var assembly = Assembly.GetCallingAssembly();
            CacheAssembly(assembly);
        }

        public static void CacheAssembly(Assembly assembly)
        {
            if (_assemblies.Contains(assembly))
                return;

            _assemblies.Add(assembly);

            var types = assembly.GetTypes();
            _types.AddRange(types);

            for (int i = 0; i < types.Length; i++)
            {
                var type = types[i];
                CacheType(type);
            }
        }

        public static TypeID CacheType(Type type)
        {
            var typeID = new TypeID(type.AssemblyQualifiedName.GetHashCode());
            _typeForCode.Add(type, typeID);
            _codeForType.Add(typeID, type);
            if(type.IsValueType)
                _typeSizes.Add(typeID, UnsafeUtility.SizeOf(type));
            return typeID;
        }

        public static Type[] GetInterfaces(Type type)
        {
            Type[] interfaces;
            if(!_interfaces.TryGetValue(type, out interfaces))
                _interfaces[type] = interfaces = type.GetInterfaces();

            return interfaces;
        }

        public static IList<Type> GetImplementors(Type type)
        {
            if (_implementors.TryGetValue(type, out IList<Type> types))
                return types;

            types = new FastList<Type>();

            for (int i = 0; i < _types._Count; i++)
            {
                var t = _types._Data[i];
                if (type.IsAssignableFrom(t))
                {
                    types.Add(t);
                }
            }

            _implementors.Add(type, types);

            return types;
        }

        public static Type GetTypeFromID(TypeID id)
        {
            _codeForType.TryGetValue(id, out Type type);
            return type;
        }

        public static TypeID GetIDForType(this Type type)
        {
            if (!_typeForCode.TryGetValue(type, out TypeID typeID))
            {
                typeID = CacheType(type);
            }
            return typeID;
        }

        public static int GetTypeSize(TypeID typeID)
        {
            _typeSizes.TryGetValue(typeID, out int size);
            return size;
        }

        public static int GetTypeSize(Type type)
        {
            if (!_typeSizes.TryGetValue(type.GetIDForType(), out int size))
            {
                var typeID = CacheType(type);
                _typeSizes.TryGetValue(typeID, out size);
            }
            return size;
        }
    }
}
